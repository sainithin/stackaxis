const express = require("express"),
app = express(),
bodyParser = require("body-parser"),

mongoose = require("mongoose"),
flash = require("connect-flash"),
session = require("express-session"),
moment = require("moment"),
passport = require("passport"),
LocalStrategy = require("passport-local"),
methodOverride = require("method-override"),
async      = require("async"),
middleware = require("./middleware"),
nodemailer = require("nodemailer"),

Support = require("./models/support");
User = require("./models/user");
Super = require("./models/superuser");


let shell = require('shelljs');
var a,id;




app.get("/", (req, res) => res.render("home"));





// requiring routes



 let url = process.env.DATABASEURL || "mongodb://localhost/stackaxis";
 mongoose.connect(url, { useNewUrlParser: true })
    .then(() => console.log(`Database connected`))
.catch(err => console.log(`Database connection error: ${err.message}`));

// connect to the DB
 //const databaseUri = 'mongodb+srv://stackaxis:StackAxis123@cluster0-6cd7z.mongodb.net/test?retryWrites=true&w=majority'
//mongoose.connect(databaseUri,{ useNewUrlParser: true })
  //    .then(() => console.log(`Database connected`))
  //.catch(err => console.log(`Database connection error: ${err.message}`));



app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public"));
app.use(methodOverride("_method"));
app.use(flash());
app.locals.moment = moment; // create local variable available for the application

//passport configuration
app.use(session({
  secret: 'abcd',
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use('user',new LocalStrategy(User.authenticate()));
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  if(user!=null)
    done(null,user);
});



// pass currentUser to all routes
app.use((req, res, next) => {
  res.locals.currentUser = req.user; // req.user is an authenticated user
  res.locals.error = req.flash("error");
  res.locals.success = req.flash("success");
  next();
});

// use routes

// app.get('/account',middleware.isLoggedIn, (req, res) => {


//       User.findByIdAndUpdate(id).exec(function (err, list) {
//         if (err) {
//             console.log(err);
//         } else {
//             list.ipad = a.stdout;
//             list.save();
//         }
//     })
//     User.find({}, function (err, list) {
//         if (err) {
//             console.log(err);
//         } else {
//             res.render("account", { list: list,a:a.stdout });
//         }
//     });
// });
// use routes




// var collectionOne = [];
// var collectionThree = [];

// app.get('/account',  function(req, res){



//   User.find({}, function(err, result) {
//     if (err) {
//       throw err;
//   } else {
//       for (i=0; i<result.length; i++) {
//         collectionOne[i] = result[i];
//     }
// }
// });

//     User.findByIdAndUpdate(id).exec(function (err, list) {
//         if (err) {
//             console.log(err);
//         } else {
//             list.ipad = a.stdout;
//             list.save();
//         }
//     })



//   res.render('account', {
//     collectionOne: collectionOne,
//     a:a.stdout,
// });
// });













// app.get('/account',middleware.isLoggedIn, (req, res) => {



//     User.find({}, function (err, list) {
//         if (err) {
//             console.log(err);
//         } else {
//             res.render("account", { list: list,a:a.stdout });
//         }
//     });

//       User.find({}, function (err, list) {
//         if (err) {
//             console.log(err);
//         } else {
//             res.render("account", { list: list,a:a.stdout });
//         }
//     });

// });


var collectionOne = [];
var collectionTwo = [];

// app.get('/account',middleware.isLoggedIn,  function(req, res){


//  User.findByIdAndUpdate(id).exec(function (err, list) {
//         if (err) {
//             console.log(err);
//         } else {
//             list.ipad = a.stdout;
//             list.save();
//         }
//     })
//   User.find({}, function(err, result) {
//     if (err) {
//       throw err;
//   } else {
//       for (i=0; i<result.length; i++) {
//         collectionOne[i] = result[i];
//     }
// }
// });
//   Support.find({}, function(err, result) {

//     if (err) {
//       throw err;
//   } else {
//       for (i=0; i<result.length; i++) {
//         collectionTwo[i] = result[i];
//     }
// }
// });

//   res.render('account', {
//     list: collectionOne,
//     collectionTwo: collectionTwo,
//     a:a.stdout
// });
// });



app.get('/account',middleware.isLoggedIn, (req, res) => {



    Support.find({}, function (err, list) {

        if (err) {
            console.log(err);
        } else {
          console.log(list);
           for (i=0; i<list.length; i++) {
        collectionTwo[i] = list[i];
    }
     User.find({}, function(err, result) {
    if (err) {
      throw err;
  } else {
res.render('account', {
    list: result,
    collectionTwo: collectionTwo,
    a:a.stdout
});

    // console.log(result);
    // console.log("\n");
    //    console.log(collectionTwo);



}
});


        }
    });
});


app.get("/add_user",middleware.isLoggedIn, (req, res) => res.render("add"));

app.get('/add_table', (req, res) => {
    User.find({}, function (err, list) {
        if (err) {
            console.log(err);
        } else {
            res.render("add_table", { list: list });
        }
    });
});

app.post("/login", (req, res, next) => {
    passport.authenticate("user", (err, user, info) => {
        if (err) { return next(err); }
        if (!user) {
            req.flash("error", "Invalid username or password");
            return res.redirect('/');
        }
        req.logIn(user, err => {
            if (shell.which('git')) {

    a=shell.exec('curl ifconfig.me');

    console.log(a.stdout);

      } else {

      console.log('you do not have git installed.');

      }
            if (err) { return next(err); }
            let redirectTo = req.session.redirectTo ? req.session.redirectTo : '/account';
            delete req.session.redirectTo;
            id=user.id;
            req.flash("success", "Good to see you again, " + user.name);
            res.redirect(redirectTo);
        });
    })(req, res, next);
});


app.get("/logout", (req, res) => {
    req.logout();
    req.flash("success", "Logged out successfully. Looking forward to seeing you again!");
    res.redirect("/");
});


app.post("/register", (req, res) => {
    let newUser = new User({
        username: req.body.username,
    });

    if (req.body.adminCode == 8080) {
        newUser.isSuper = true;
    }
    else {
        req.flash("error", "Entered Wrong code contact Administrator");
        return res.redirect("/register");
    }
    User.register(newUser, req.body.password, (err, user) => {
        if (err) {
            if (err.email === 'MongoError' && err.code === 11000) {
                // Duplicate email
                req.flash("error", "That email has already been registered.");
                return res.redirect("/register");
            }
            // Some other error
            req.flash("error", "Something went wrong...");
            return res.redirect("/register");
        }

        passport.authenticate("local")(req, res, () => {
            req.flash("success", "Welcome  " + user.username);
            res.redirect("/account");
        });
    });
});



app.get('/add/:name',middleware.isLoggedIn, (req, res) => {

    Super.find({}, function (err, list) {
        if (err) {
            console.log(err);
        } else {
            res.render("client_details", { list: list,name:req.params.name });
        }
    });
});


app.get("/register", (req, res) => res.render("register"));
// handle sign up logic
app.post("/adduser", (req, res) => {
    var email=req.body.email;
       username= req.body.username;
        password=req.body.password;
          gateway= req.body.gateway;
        port=req.body.port;
         exprirydate=req.body.exprirydate;
         ipad=req.body.ipad;
         skype=req.body.skype;
         cname=req.body.cname;
         bhw=req.body.bhw;
         mps=req.body.mps;
    let newUser = new User({
        username: req.body.username,
        password:req.body.password,
        email: req.body.email,
           gateway:req.body.gateway,
        port:req.body.port,
         exprirydate:req.body.exprirydate,
         ipad:req.body.ipad,
         skype:req.body.skype,
         cname:req.body.cname,
         bhw:req.body.bhw,
         mps:req.body.mps,
         isadmin:"false",
         createdBy:req.body.createdby
    });

    User.register(newUser, req.body.password, (err, user) => {
        if (err) {
            if (err.email === 'MongoError' && err.code === 11000) {
                // Duplicate email
                req.flash("error", "That email has already been registered.");
                return res.redirect("/register");
            }
            // Some other error
            req.flash("error", "Something went wrong...");
            return res.redirect("/register");
        }

        res.redirect("/account");
    });
});



app.post("/add_user", (req, res) => {
      var username= req.body.username;
          gateway= req.body.gateway;
        port=req.body.port;
         expirydate=req.body.expirydate;
         ipad = req.body.ipad;
         pusername = req.body.pusername;
         ppassword = req.body.ppassword;
    let newUser = new User({
        username: req.body.username,
           gateway:req.body.gateway,
        port:req.body.port,
         expirydate:req.body.expirydate,
         ipad:req.body.ipad,
         pusername:req.body.pusername,
         ppassword:req.body.ppassword,
         createdBy:req.body.createdby
    });
      Super.create(newUser, function (err, newlyCreated) {
        if (err) {
            console.log(err);
        } else {
            console.log(newUser);
            res.redirect("/account");
        }
    })
});


app.post('/update/:id', function (req, res) {


    var gateway=req.body.gateway;
    var port=req.body.port;
    var exprirydate =req.body.exprirydate;
    var ipad = req.body.ipad;



   User.findByIdAndUpdate(req.params.id).exec(function (err, list) {
        if (err) {
            console.log(err);
            res.redirect('/account');
        } else {


              list.gateway = gateway;
              list.port = port;
              list.exprirydate = exprirydate;
              list.ipad=ipad;
              list.save();
              res.redirect('/account');
        }
    });
  });



// app.get("/support",(req, res) => res.render("support"));
app.post('/support', function (req, res) {

    var name=req.body.name;
    var phone=req.body.phone;
    var email=req.body.email;
    var query =req.body.query;
    var newQuery = { username: name, phone: phone, email: email, query: query }
    Support.create(newQuery, function (err, newlyCreated) {
        if (err) {
            console.log(err);
        } else {
            req.flash("success", newQuery.name + " Our customer service will contact you soon !!");
            res.redirect("/support");
        }
    })
  });

app.post('/closed', function (req, res) {

    var name=req.body.username;
  Support.findOneAndUpdate({username: name}, {$set:{status:"Closed"}},function(err, doc){
    if(err){
        console.log("Something wrong when updating data!");
    }
    else{
 res.redirect("/supportrequests");
    }

});
  });



app.get('/support',middleware.isLoggedIn, (req, res) => {



    Support.find({}, function (err, list) {
        if (err) {
            console.log(err);
        } else {
            res.render("support", { list: list });
        }
    });
});


app.get('/supportrequests',middleware.isLoggedIn, (req, res) => {



    Support.find({}, function (err, list) {
        if (err) {
            console.log(err);
        } else {
            res.render("supportrequests", { list: list });
        }
    });
});

// app.get("/supportrequests",(req, res) => res.render("supportrequest"));
app.post('/support', function (req, res) {

    var name=req.body.name;
    var phone=req.body.phone;
    var email=req.body.email;
    var query =req.body.query;
    var newQuery = { username: name, phone: phone, email: email, query: query }
    Support.create(newQuery, function (err, newlyCreated) {
        if (err) {
            console.log(err);
        } else {
            req.flash("success", newQuery.name + " Our customer service will contact you soon !!");
            res.redirect("/support");
        }
    })
  });



app.get("/super_user", (req, res) => res.render("super_user"));



app.get("/add_admin", (req, res) => res.render("add_admin"));
// handle sign up logic
app.post("/add_admin", (req, res) => {
    var email=req.body.email;
       username= req.body.username;
        password=req.body.password;
          gateway= req.body.gateway;
        port=req.body.port;
         exprirydate=req.body.exprirydate;
    let newUser = new User({
        username: req.body.username,
        password:req.body.password,
        email: req.body.email,
           gateway:req.body.gateway,
        port:req.body.port,
         exprirydate:req.body.exprirydate,
         isAdmin:"true",
         createdBy:req.body.createdby
    });

    User.register(newUser, req.body.password, (err, user) => {
        if (err) {
            if (err.email === 'MongoError' && err.code === 11000) {
                // Duplicate email
                req.flash("error", "That email has already been registered.");
                return res.redirect("/register");
            }
            // Some other error
            req.flash("error", "Something went wrong...");
            return res.redirect("/register");
        }

        res.redirect("/account");
    });
});


app.listen((process.env.PORT || 3030), function () {
  console.log("The Server Has Started!");
});
