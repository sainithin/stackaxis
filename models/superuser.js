const mongoose = require("mongoose");

const ClientSchema = new mongoose.Schema({
    username: { type: String},
    gateway:String,
    port:String,
    expirydate:{type:Date},
    ipad:String,
    pusername:String,
    ppassword:String,
    skype: String,
    cname: String,
    bhw: String,
    mps:String,
    createdBy:String
});

module.exports = mongoose.model("Super", ClientSchema);
