const mongoose = require("mongoose"),
    passportLocalMongoose = require("passport-local-mongoose");

const UserSchema = new mongoose.Schema({
    username: { type: String},
    password: String,
    email: { type: String},
    gateway:String,
    port:String,
    expirydate:{type:Date},
    pusername:String,
    ppassword:String,
    skype: String,
    cname: String,
    bhw: String,
    mps:String,
    ipad:String,
    isAdmin:{type:String,default:false},
    isSuper:{type:String,default:false},
    createdBy:String
});





UserSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("User", UserSchema);
